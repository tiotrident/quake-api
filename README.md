# Quake Parser

Quake Parser é uma api em node de duas rotas que utiliza um Parser para estruturar um log do game quake arena 3 e retorna em formato json.

## Instalação

#### Instalação de dependências

Recomenda-se fazer a utilização do package manager [yarn](https://yarnpkg.com/getting-started/install) para instalar as dependências do Quake Parser.

```bash
yarn install
```

#### Variável de abiante

```
Dentro de um arquivo .env na pasta principal contém todas as variáveis de ambiente utilizada.

##EXEMPLO:

PORT=               | Porta que deseja executar a api
SECRET=             | Palavra chave utilizada na criptografia do token (usar https://jwt.io/ para gerar o token)
NODE_ENV=           | Ambiente de trabalho (Caso use yarn test ele usa automaticamente test, caso vazio "Development".)

```

#### Variável global Jest

```bash
Dentro do arquivo "jest.config" no campo "globals" se encontra as variáveis globais utilizada para testes.
```

## Execução

**Start service**: Para iniciar a API

```
yarn start
```

**Development mode**: Para iniciar a API em modo desenvolvimento com nodemon

```
yarn dev
```

**Start service**: Para iniciar a API em modo de test (jest)

```
yarn test
```

## Rotas

**Check Healt**: Rota destinada a teste de tempo de resposta

```
GET https://localhost:3000/ping
```

**Game report**: Rota destinada a retorno do relatório contido no game.log

```
GET https://localhost:3000/games/
```

**Autorização de rota**
Como metodo de autorização foi adotado a autenticação bearer.Para acessar a rota GAME REPORT requer token.

Token no header:

```
"Authorization": "Bearer {[TOKEN}}"
```

Token no Postman
![Exemplo Postman](https://i.imgur.com/AudcqOE.png)

## Estrutura de arquivos

```
QUAKE-API
    ├── common
    |       ├── controller.js
    |       └── error.js
    ├── config
    │       └── environment.js
    ├── coverage
    |       └── . . .
    ├── database
    |       ├── emptyGame.test.log
    |       ├── games.log
    |       └── games.test.log
    ├── node_modules
    |       └── . . .
    ├── server
    |       ├── error.handle.js
    |       ├── security.middleware.js
    |       └── server.js
    ├── src
    |   ├── games
    |   |       └── __test__
    |   |       |        └── __mocks__
    |   |       |            ├── games.mock.js
    |   |       |            └── response.js
    |   |       ├── games.business.js
    |   |       ├── games.controller.js
    |   |       ├── games.repository.js
    |   |       └── games.routes.js
    |   └── ping
    |       ├── __test__
    |       |        └── integration
    |       |               └── ping.test.js
    |       └── ping.routes.js
    ├── .env
    ├── .gitpod.yml
    ├── index.js
    ├── jest.config.js
    ├── jest.statup.js
    ├── package.json
    ├── README.md
    └── yarn.lock
```

## Observações

_Pipelines de Integração Contínua com o GitLab já configurado._

Repositório dedicado a prova LuizaLabs Quake Log.

## License

[MIT](https://choosealicense.com/licenses/mit/)
