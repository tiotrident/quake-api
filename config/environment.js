const environment = {
  application: {
    port: process.env.PORT || 3000,
    secret: process.env.SECRET || "provaMagalu",
    nodeEnv: process.env.NODE_ENV || "development"
  }
};

module.exports = environment;
