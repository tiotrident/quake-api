const server = require('./server/server');

server
    .bootstrap()
    .then(_ => console.info("Server has been started"))
    .catch(err => {
        console.error(err);
        process.exit(1);
});