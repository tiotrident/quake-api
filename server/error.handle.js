const { NOT_FOUND, INTERNAL_SERVER_ERROR } = require("http-status");
const { NotFound } = require("../common/error");

function errorHandler(err, req, res, next) {
    console.error(err);
  
    let code = INTERNAL_SERVER_ERROR;
    let message = "Internal Server Error";
  
    if (err instanceof NotFound) {
      code = NOT_FOUND;
      message = err.message;
    }
  
    res.status(code).json({
      code,
      message
    });
  }
  
  module.exports = errorHandler;
  