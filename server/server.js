const express = require("express");

const environment = require("../config/environment");
const errorHandler = require("./error.handle");

const pingRoutes = require("../src/ping/ping.routes");
const gameReportRoutes = require("../src/games/games.routes");

class Server {
  constructor() {
    this._app = express();
  }

  _init() {
    return new Promise((resolve, reject) => {
      try {
        this._app.use(express.json());

        this._app.use(errorHandler);
        this._app.use("/", pingRoutes);
        this._app.use("/games", gameReportRoutes);
        this._app.listen(environment.application.port, resolve);
      } catch (err) {
        reject(err);
      }
    });
  }

  async bootstrap() {
    await this._init();
  }

  shutdown() {
    process.exit(0);
  }
}

const server = new Server();
module.exports = server;
