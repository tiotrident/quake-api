const gameApiResponse = [
  {
    Game: {
      total_kills: 11,
      players: ["Isgalamido", "Dono da Bola", "Mocinha"],
      kills: {
        Isgalamido: -3
      },
      id: 1
    }
  }
];

module.exports = {
  gameApiResponse
};
