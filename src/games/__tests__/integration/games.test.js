const request = require("supertest");

const address = global["address"];
const mockResponse = require("../__mocks__/games.mock");

describe("check route of game report", () => {
  it("You must return a code status of 200 and a report for all games shown in the log", async () => {
    const res = await request(address)
      .get("/games")
      .set(
        "Authorization",
        "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.kk5T2cf7dR_1lDq2mUXKZwi4okQIz6zonaWCT5elMz8"
      );
    expect(res.statusCode).toEqual(200);
    expect(res.type).toEqual("application/json");
    expect(res.body).toEqual(mockResponse.gameApiResponse);
  });
});
