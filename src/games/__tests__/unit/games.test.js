const fs = require("fs");

const gamesController = require("../../games.controller");
const gamesBussiness = require("../../games.business");
const gamesRepository = require("../../games.repository");

// const gamesMock = require("../../../../database/games.test.log");

describe("test parser controller functions", () => {
  test("gameParser", () => {
    const res = gamesController.gameParser();
  });

  test("parse success", () => {
    const res = gamesBussiness.parse();
  });

  //   test("parse all log and get success", async () => {
  //     const archiveLog = await fs
  //       .readFileSync("./database/games.log", "utf8")
  //       .toString();
  //     const res = gamesRepository.logParser(archiveLog);
  //     expect(res).toEqual(gamesMock);

  test("parse all log and get error for empty game log", async () => {
    const archiveLog = await fs
      .readFileSync("./database/emptyGame.test.log", "utf8")
      .toString();
    const res = gamesRepository.logParser(archiveLog);
    expect(res).toEqual(Error("Oooops! Empty game log!"));
  });
});
