const fs = require("fs");

const gamesRepository = require("./games.repository");
const environment = require("../../config/environment");

class GamesBussiness {
  async parse() {
    try {
      const path =
        environment.application.nodeEnv === "test"
          ? "./database/games.test.log"
          : "./database/games.test.log";
      const gameList = await gamesRepository.logParser(
        fs.readFileSync(path, "utf8").toString()
      );

      return gameList;
    } catch (err) {
      console.log(err);
    }
  }
}

const gamesBussiness = new GamesBussiness();
module.exports = gamesBussiness;
