const gamesBussiness = require("./games.business");
const Controller = require("../../common/controller");

class ParserController extends Controller {
  gameParser() {
    return (req, res, next) => {
      gamesBussiness
        .parse()
        .then(this.ok(res, next))
        .catch(next);
    };
  }
}

const gamesController = new ParserController();
module.exports = gamesController;
