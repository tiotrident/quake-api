class GamesRepository {
  logParser(archive) {
    try {
    } catch (error) {}
    let gameList = [];
    const logLines = archive.split("\n");
    if (logLines <= 1) {
      return Error("Oooops! Empty game log!");
    }

    logLines.forEach(line => {
      checkCommand(line);
    });

    function checkCommand(line) {
      let logLine = line.trim().split(" ");
      let command = logLine[1];

      switch (command) {
        case "InitGame:":
          createNewGame(gameList);
          break;
        case "Kill:":
          verifyKills(line);
          break;
        case "ClientUserinfoChanged:":
          createPlayer(line);
          break;
        default:
      }
    }
    function createNewGame(list) {
      list.push({
        Game: {
          total_kills: 0,
          players: [],
          kills: {}
        }
      });
    }

    function verifyKills(line) {
      let match = gameList[gameList.length - 1];
      let killer = line.trim().split(" ")[5];
      if (killer.includes("<world>")) {
        let strKilledPosition = line.indexOf("killed");
        let strByPosition = line.indexOf("by");
        let nicknameLenght = strByPosition - strKilledPosition;
        let killed = line
          .substr(strKilledPosition, nicknameLenght)
          .replace("killed ", "")
          .trim();

        match.Game.kills[killed] = match.Game.kills[killed] - 1 || 0;
      } else {
        playerKill(killer);
      }
      match.Game.total_kills++;
    }

    function playerKill(killer) {
      let match = gameList[gameList.length - 1];
      match.Game.kills[killer] = match.Game.kills[killer] + 1 || 1;
    }

    function createPlayer(line) {
      let startName = line.indexOf("n\\") + 2;
      let endName = line.indexOf("\\t");
      let nicknameLenght = endName - startName;
      let nickname = line.substr(startName, nicknameLenght);

      if (gameList[gameList.length - 1].Game.players.indexOf(nickname) === -1) {
        gameList[gameList.length - 1].Game.players.push(nickname);
      }
    }

    gameList.map((report, index) => {
      report.Game.id = index + 1;
    });

    return gameList;
  }
}
const gamesRepository = new GamesRepository();
module.exports = gamesRepository;
