const express = require("express");
const router = express.Router();
const { OK } = require("http-status");

router.get("/ping", (req, res) => res.status(OK).send("pong"));

module.exports = router;
